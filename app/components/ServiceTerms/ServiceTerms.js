import React from 'react';
import './style.scss';

export default class ServiceTerms extends React.Component {
    render() {
        return (
            <div className="tsp-container">
                <div className="page-title">
                    <h2>
                        <center>LEXHEAL Terms of Service</center>
                    </h2>
                    <h4>
                        <center>lexheal.com / Lexheal</center>
                    </h4>
                    <p>
                        <center>Terms of Use</center>
                    </p>
                    <p className="cautionary-text">
                        <i>PLEASE READ THE TERMS OF USE CAREFULLY BEFORE AVAILING SERVICES ON THIS PLATFORM. AVAILING SERVICES THROUGH THIS PLATFORM SIGNIFIES YOUR ACCEPTANCE OF THE TERMS OF USE AND YOUR AGREEMENT TO BE LEGALLY BOUND BY THE SAME.
                        IF YOU DO NOT AGREE WITH THE TERMS OF USE, PLEASE DO NOT ACCESS THIS PLATFORM FOR ANY SERVICES.
                        </i>
                    </p>
                </div>  
                <div className="page-heading-point">
                    <h4>1. INTRODUCTION</h4>
                    <div className="page-points">
                       1.1 )  www.lexheal.com / [Lexheal] (hereinafter referred to as “Lexheal”, “Lexheal Application”, “Website” or the “Platform”, which terms shall mean and include the website, sub-domains of the website, software, mobile application(s) or any other medium through which Lexheal may provide its services) is owned and operated by Lexheal Technologies Private Limited , a Company incorporated under the provisions of the (Indian) Companies Act, 2013 having its registered office at No. 9/13, Ward No. 16, Kurubarahally Main Road, Shankar Mutt,, BANGALORE, Bangalore, Karnataka, India, 560086.
                    </div>
                    <div className="page-points">
                       1.1 )  www.lexheal.com / [Lexheal] (hereinafter referred to as “Lexheal”, “Lexheal Application”, “Website” or the “Platform”, which terms shall mean and include the website, sub-domains of the website, software, mobile application(s) or any other medium through which Lexheal may provide its services) is owned and operated by Lexheal Technologies Private Limited , a Company incorporated under the provisions of the (Indian) Companies Act, 2013 having its registered office at No. 9/13, Ward No. 16, Kurubarahally Main Road, Shankar Mutt,, BANGALORE, Bangalore, Karnataka, India, 560086.
                    </div>
                    <div className="page-points">
                       1.1 )  www.lexheal.com / [Lexheal] (hereinafter referred to as “Lexheal”, “Lexheal Application”, “Website” or the “Platform”, which terms shall mean and include the website, sub-domains of the website, software, mobile application(s) or any other medium through which Lexheal may provide its services) is owned and operated by Lexheal Technologies Private Limited , a Company incorporated under the provisions of the (Indian) Companies Act, 2013 having its registered office at No. 9/13, Ward No. 16, Kurubarahally Main Road, Shankar Mutt,, BANGALORE, Bangalore, Karnataka, India, 560086.
                    </div>
                    <div className="page-points">
                       1.1 )  www.lexheal.com / [Lexheal] (hereinafter referred to as “Lexheal”, “Lexheal Application”, “Website” or the “Platform”, which terms shall mean and include the website, sub-domains of the website, software, mobile application(s) or any other medium through which Lexheal may provide its services) is owned and operated by Lexheal Technologies Private Limited , a Company incorporated under the provisions of the (Indian) Companies Act, 2013 having its registered office at No. 9/13, Ward No. 16, Kurubarahally Main Road, Shankar Mutt,, BANGALORE, Bangalore, Karnataka, India, 560086.
                    </div>
                    <div className="page-points">
                       1.1 )  www.lexheal.com / [Lexheal] (hereinafter referred to as “Lexheal”, “Lexheal Application”, “Website” or the “Platform”, which terms shall mean and include the website, sub-domains of the website, software, mobile application(s) or any other medium through which Lexheal may provide its services) is owned and operated by Lexheal Technologies Private Limited , a Company incorporated under the provisions of the (Indian) Companies Act, 2013 having its registered office at No. 9/13, Ward No. 16, Kurubarahally Main Road, Shankar Mutt,, BANGALORE, Bangalore, Karnataka, India, 560086.
                    </div>
                    <div className="page-points">
                       1.1 )  www.lexheal.com / [Lexheal] (hereinafter referred to as “Lexheal”, “Lexheal Application”, “Website” or the “Platform”, which terms shall mean and include the website, sub-domains of the website, software, mobile application(s) or any other medium through which Lexheal may provide its services) is owned and operated by Lexheal Technologies Private Limited , a Company incorporated under the provisions of the (Indian) Companies Act, 2013 having its registered office at No. 9/13, Ward No. 16, Kurubarahally Main Road, Shankar Mutt,, BANGALORE, Bangalore, Karnataka, India, 560086.
                    </div>
                    <div className="page-points">
                       1.1 )  www.lexheal.com / [Lexheal] (hereinafter referred to as “Lexheal”, “Lexheal Application”, “Website” or the “Platform”, which terms shall mean and include the website, sub-domains of the website, software, mobile application(s) or any other medium through which Lexheal may provide its services) is owned and operated by Lexheal Technologies Private Limited , a Company incorporated under the provisions of the (Indian) Companies Act, 2013 having its registered office at No. 9/13, Ward No. 16, Kurubarahally Main Road, Shankar Mutt,, BANGALORE, Bangalore, Karnataka, India, 560086.
                    </div>
                </div>

                <div className="page-heading-point">
                    <h4>II. SCOPE OF SERVICE</h4>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points text-bold text-underline">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    
                </div>



                <div className="page-heading-point">
                    <h4>III. ACCESS AND TRANSMISSION OF CONTENTS OF THE PLATFORM</h4>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points text-bold text-underline">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    
                </div>


                <div className="page-heading-point">
                    <h4>IV. PRIVACY POLICY</h4>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points text-bold text-underline">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    
                </div>


                <div className="page-heading-point">
                    <h4>V. DISCLAIMER OF WARRANTIES</h4>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points text-bold text-underline">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    
                </div>

                <div className="page-heading-point">
                    <h4>VI. INDEMNITY</h4>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points text-bold text-underline">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    <div className="page-points">
                    The Platform presently comprises a mobile application (Lexheal) that allows users to create health profiles in order to manage and monitor their health parameters and healthcare related information. Users can track important health parameters such as vaccination, growth charts, milestones, blood pressure, blood sugar etc. (illustrative list). Users can also privately upload their prescriptions, medical reports, medical bills, etc. and store and retrieve the same through the Platform’s secure cloud-based servers.
                    </div>
                    
                </div>


                



            </div>   
        );
    }
}