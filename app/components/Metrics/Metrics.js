import React from "react";
import "./style.scss";

class Metrics extends React.Component {
  render() {
    return (
      <div className="download-status-container">
        <div className="download-circle circle align-items-center">
           <svg width="200" height="200">d
            <circle
              cx="100"
              cy="100"
              r="50"
              stroke="#E4A307"
              // stroke-width="4"
              fill="white"
            />
            Sorry, your browser does not support inline SVG.
            <text
              fill="#000000"
              // font-size="18"
              // font-family="Verdana"
              x="80"
              y="110"
            >
              400+
            </text>
          </svg> 
          <p className="download-text">Downloads</p>
        </div>
      </div>
    );
  }
}

export default Metrics;
