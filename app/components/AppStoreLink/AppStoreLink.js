import React from 'react';
import './style.scss';
import AndroidLinkImage from '../../images/google-play-badge.png'

const AppStoreLink = () => (
    <div className="play-store-wrapper">
        <a href="https://play.google.com/store/apps/details?id=com.lexheal" target="_blank">
            <img alt="lexheal-app-download" src={AndroidLinkImage}/>
        </a>
    </div>
   
);

export default AppStoreLink;
