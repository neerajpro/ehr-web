import React from 'react';
import './style.scss';
import Navigation from '../Navigation';

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <header>
        <Navigation></Navigation>
        <div className="top-right-image"></div>
      </header>
    );
  }
}

export default Header;
