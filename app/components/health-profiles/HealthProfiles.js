import React from 'react';
import './styles.scss';
import healthProfileImage from '../../images/health-profile-circle.svg'

class HealthProfiles extends React.Component {
   render(){
      return(
         <div className="health-profiles">
            <div className="profile-icon"></div>
            <div className="profile-intro"> 
               <span>{this.props.profileName}</span>
            </div>
         </div>
      )
   }   
}

export default HealthProfiles;
