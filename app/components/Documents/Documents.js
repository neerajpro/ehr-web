import React from "react";
import "./style.scss";

class Documents extends React.Component {
  render() {
    return (
      <div className="lh-documents-section">
        <div className="columns lh-documents-cols">
          <div className="column is-half documents-left">
            <div className="features-faq"></div>
          </div>
          <div className="column is-half documents-right">
            <div className="document-1">
              <div className="columns">
                <div className="column is-one-quarter track-growth-image"></div>
                <div className="column is-three-quaters">
                  <h1>Track your Growth</h1>
                  <p>
                    Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
                    Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
                    Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
                    Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
                    Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
                    Lorem ipsum
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Documents;
