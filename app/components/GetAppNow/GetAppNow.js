import React from "react";
import "./style.scss";
import AppStoreLink from "../AppStoreLink/AppStoreLink";

class GetAppNow extends React.Component {
    render() {
        return(
            <div className="lh-get-app-section">
            <div className="columns lh-get-app-section-cols">
              <div className="column is-two-thirds get-app-section-left">
                <div className="get-app-1">
                  <div className="columns">
                    <div className="column">
                      <h1>Get your App Now</h1>
                      <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
                      <div className="android-app-store-link">
                          <AppStoreLink/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="column get-app-right"></div>
            </div>
          </div>
        )
    }
}

export default GetAppNow;