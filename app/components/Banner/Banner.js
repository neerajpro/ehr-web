import React from "react";
import "./style.scss";
import AppStoreLink from "../AppStoreLink";
import HealthProfiles from "../health-profiles/HealthProfiles";

export default class Banner extends React.Component {
  render() {
    const listOfProfiles = ["Diabetics", "Profile2", "Profile3", "Profile4"];
    return (
      <div className="lh-banner-section">
        <div className="lh-home-section">
          <div className="columns lh-home-cols">
            <div className="column is-half lh-introduction">
              <div className="intro-box">
                <h1>Know Your Health Better</h1>
                <p className="intro-text">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor.
                </p>
                <div className="android-playstore">
                  <AppStoreLink />
                </div>
              </div>
            </div>
            <div className="column is-half family-banner"></div>
          </div>
        </div>
        <div className="lh-profile-section">
          {listOfProfiles.map(listOfProfiles => (
            <HealthProfiles key={listOfProfiles} profileName={listOfProfiles} />
          ))}
        </div>
      </div>
    );
  }
}
