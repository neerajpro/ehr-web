import React from "react";
import Logo from "../logo";
import "./style.scss";
import { configureAnchors } from 'react-scrollable-anchor'
import { removeHash } from 'react-scrollable-anchor'

export class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = { addClass: false };
  }

  componentWillMount() {
    configureAnchors({offset: -65, scrollDuration: 200});
    removeHash();
  }

  toggle() {
    this.setState({ addClass: !this.state.addClass });
  }

  render() {
    return (
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-fixed-top">
          <div className="logo-placeholder">
            <Logo />
          </div>
          <a
            role="button"
            className={`navbar-burger ${
              this.state.addClass ? "is-active" : ""
            } `}
            onClick={this.toggle.bind(this)}
            aria-label="menu"
            aria-expanded="false"
            data-target="#navMenu"
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>
        <div
          className={`navbar-module navbar-menu ${
            this.state.addClass ? "is-active" : ""
          }`}
          id="navMenu"
        >
          <div className="navbar-end">
            <span className="navbar-item">
              <a className="link"  href='#home'>
                Home
              </a>
            </span>
            <span className="navbar-item">
              <a className="link" href='#features'>
                Features
              </a>
            </span>
            <span className="navbar-item">
              <a className="link" href='#services'>
                Services
              </a>
            </span>
            <span className="navbar-item">
              <a className="link" href='#contact-us'>
                Contact
              </a>
            </span>
          </div>
        </div>
      </nav>
    );
  }
}
export default Navigation;
