import React from "react";
import "./style.scss";

class Trackers extends React.Component {
    render() {
        return(
            <div className="lh-tracker-section">
            <div className="columns lh-trackers-cols">
              <div className="column is-half trackers-left">
                <div className="tracker-1">
                  <div className="columns">
                    <div className="column is-one-quarter track-growth-image"></div>
                    <div className="column is-three-quaters">
                      <h1>Track your Growth</h1>
                      <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="column is-half trackers-right">
                <div className="features-faq"></div>
              </div>
            </div>
          </div>
        );
    }
}

export default Trackers;