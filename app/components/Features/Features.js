import React from "react";
import "./style.scss";

class Features extends React.Component {
  render() {
    return (
      <div className="lh-features-section">
        <div className="columns lh-features-cols">
          <div className="column is-one-third features-left">
            <div className="features-faq"></div>
          </div>
          <div className="column is-two-thirds features-right">
            <h3>Some of the best features</h3>
            <div className="feature-1">
              <div className="columns">
                <div className="column is-one-quarter track-growth-image"></div>
                <div className="column is-three-quaters track-growth-text">
                  <h1>Track your Growth</h1>
                  <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
                </div>
              </div>
            </div>

            <div className="feature-2">
              <div className="columns">
                <div className="column is-one-quarter track-growth-image"></div>
                <div className="column is-three-quaters track-growth-text">
                  <h1>Track your Growth</h1>
                  <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Features;
