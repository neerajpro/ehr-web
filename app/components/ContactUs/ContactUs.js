import React from 'react';
import "./style.scss";
import Footer from '../Footer/Footer';
class ContactUs extends React.Component{
    render() {
        return(
            <div className="lh-contact-us-container">
                <div className="contact-us-text">
                    <div>Contact Us</div>
                </div>  
                <div className="help-text">We would like to hear from you.</div>              
                <div className="email-address-container">
                    <span>E-mail: contact@lexheal.com</span>
                </div>
                <div className="service-terms">
                    <p><a href="/terms-of-service" target="_blank">Terms of Service</a></p>
                    <p><a href="/privacy-policy" target="_blank">Privacy Policy</a></p>
                </div>
                <Footer/>
            </div>         
        )
    }
}

export default ContactUs;