/*
 *  Created on 13th Feb 2020 ,
 */

import React from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import "./style.scss";
import Features from "../../components/Features/Features";
import Trackers from "../../components/Trackers";
import Documents from "../../components/Documents";
import GetAppNow from "../../components/GetAppNow/GetAppNow";
import Metrics from "../../components/Metrics";
import ContactUs from "../../components/ContactUs/ContactUs";
import ScrollableAnchor from 'react-scrollable-anchor'
import Banner from "../../components/Banner";
import Header from 'components/Header';

export default class HomePage extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    // keeping this method blank as may be needed in future
  }

  render() {
    const { loading, error } = this.props;

    const listOfProfiles = ["Diabetics", "Profile2", "Profile3", "Profile4"];
    return (
      <section>
        <Helmet>
          <title>Lexheal </title>
          <meta name="description" content="Lexheal | Your Health Companion" />
        </Helmet>
        <Header/>
        <div className="lh-main-container">
          

          <ScrollableAnchor id={'home'}>
              <Banner/>
          </ScrollableAnchor>

          
          <ScrollableAnchor id={'features'}>
            <Features/>
          </ScrollableAnchor>


          <ScrollableAnchor id={'services'}>
            <Trackers/>
          </ScrollableAnchor>

            <Documents/> 
            <GetAppNow/>
            {/* <Metrics/> */}
            
          <ScrollableAnchor id={'contact-us'}>
              <ContactUs/>
          </ScrollableAnchor>
    
        </div>
      </section>
    );
  }
}
HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool])
};
