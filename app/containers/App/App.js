import React from 'react';
import { Helmet } from 'react-helmet';
import { Switch, Route } from 'react-router-dom';

import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import './style.scss';
import ServiceAgreementPage from 'containers/ServiceAgreement/Loadable';
import PrivacyPolicyPage from 'containers/PrivacyPolicyPage/Loadable';

const App = () => (
  <div className="app-wrapper">
    <Helmet
      titleTemplate="%s - Lexheal"
      defaultTitle="Lexheal - Your health companion"
    >
      <meta name="description" content="Lexheal - Your health companion" />
    </Helmet>
    
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path="/terms-of-service" component={ServiceAgreementPage} />
      <Route path="/privacy-policy" component={PrivacyPolicyPage} />
      <Route path="" component={NotFoundPage} />
    </Switch>
  </div>
);

export default App;
