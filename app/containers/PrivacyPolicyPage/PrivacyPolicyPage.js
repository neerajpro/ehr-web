/**
 * Service Agreement Page 
 * Created on 14th March 2020
 */

import React from 'react';
import './style.scss';
import Logo from '../../components/logo';
import Footer from '../../components/Footer/Footer';
import PrivacyPolicy from '../../components/PrivacyPolicy/PrivacyPolicy';

export default function PrivacyPolicyPage() {
    return (
        <article>
            <header>
                <div className="terms-logo-placeholder">
                    <Logo></Logo>
                </div>
            </header>
            <section>
                <PrivacyPolicy></PrivacyPolicy>
            </section>
            <Footer></Footer>
        </article>
    );
}