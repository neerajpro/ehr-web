/**
 * Service Agreement Page 
 * Created on 13th March 2020
 */

import React from 'react';
import './style.scss';
import Logo from '../../components/logo';
import Footer from '../../components/Footer/Footer';
import ServiceTerms from '../../components/ServiceTerms';

export default function ServiceAgreementPage() {
  return (
    <article>
        <header>
            <div className="terms-logo-placeholder">
                <Logo></Logo>
            </div>
        </header>
        <section>
            <ServiceTerms></ServiceTerms>
        </section>
       <Footer></Footer>
    </article>
  );
}
